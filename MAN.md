JAVEN MAN PAGE
==============

jvn
jvn --version

Usage
=====
jvn [cmd] args
cmd are:
- init
- pom (default if install)
- jvn (reverse)
- xml2yml
- yml2xml
- json2yml
- yml2json

