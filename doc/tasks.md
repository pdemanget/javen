[x] version make it optional
[x] artifact only on file name
[x] check only one javen per folder
[ ] include from maven repo
[ ] settings.xml - move it to settings.javen.yml
[x] xml-yml generic without project transformation
[ ] JavenProject and MavenProject generated from schemas (json-schema into yaml)
[ ] packaging javen to include from repo
[ ] move files with include (assembly)
[ ] manage modules
[ ] sort yaml members
[ ] don't save empty values, empty lists

bugs
[ ] dependency with exclusion not reversed
[ ] duplicated group/version in pom


Amelioration
------------
[ ] extract xmlyaml convertion in separated module