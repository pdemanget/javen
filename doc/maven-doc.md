
download file
-------------
mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:get \
    -DrepoUrl=url \
    -Dartifact=groupId:artifactId:version

mvn org.apache.maven.plugins:maven-dependency-plugin:2.8:get -Dartifact=org.hibernate:hibernate-entitymanager:3.4.0.GA:jar:sources

deploy file
-----------

mvn deploy:deploy-file -DgroupId=com.somecompany -DartifactId=project -Dversion=1.0.0-SNAPSHOT -DgeneratePom=true -Dpackaging=jar -DrepositoryId=nexus -Durl=http://localhost:8081/nexus/content/repositories/snapshots -Dfile=target/project-1.0.0-SNAPSHOT.jar

mvn install:install-file -DgroupId=com.pdemanget.javen -DartifactId=java8 -Dversion=1.0.0 -DgeneratePom=true -Dpackaging=javen.yml  -Dfile=javen.yml



https://stackoverflow.com/questions/37543120/how-to-manually-deploy-artifacts-in-nexus-repository-manager-oss-3
curl --fail -u admin:admin123 --upload-file foo.jar 'http://my-nexus-server.com:8081/repository/my-raw-repo/'

https://jitpack.io
	com.github.heremaps:oksse:0.9.0
	
    <repositories>
	<repository>
	    <id>jitpack.io</id>
	    <url>https://jitpack.io</url>
	</repository>
    </repositories>
	
	
	https://github.com/heremaps/oksse
	
	
Other tools
-----------
JS ...
https://npm.io/package/xml-js


CI
==
manage integration of project tooling
git+docker+maven+CI+monitoring
