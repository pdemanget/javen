
Standard Plugin to manage
==========================

jar
---
Adding timestamp as part of version

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<manifestEntries>
							<Specification-Version>${maven.build.timestamp}</Specification-Version>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>