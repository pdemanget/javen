Javen usage
=====

Permet de builder avec maven sans modifier le fichier pom.xml

Installation
-----
2 installations possibles:

en cli

  javen pom
  javen install
  javen (clean install par défaut)

dans maven, en script de pré-build

sous windows: "%HOME%\mavenrc_pre.bat"
sous linux: "/etc/mavenrc" ou $HOME/.mavenrc

puis lancer maven normalement

  mvn clean install

file format
-----
Exemple sous yaml (on peut aussi utiliser json):

    artifact: "pdem:example:0.0.1-SNAPSHOT"
    name: Project example
    include: java
    compile:
     - "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.7.1"
     - "org.json:json:20160810"
     - "pl.droidsonroids.yaml:snakeyaml:1.18.2"

    plugins:

_artifact_ et _dependencies_ sont recalculés pour recréer les tags de maven.

Le nom de fichier est libre et doit simplement se terminer par .javen.yml ou .javen.json.
Donner de préférence le même nom que l'artifact pour le retrouver plus facilement.

Sous _plugins_, le code est directement converti en XML avec l'outil yaml2xml: exemple:

  plugins:
      groupId: org.apache.maven.plugins
      artifactId: maven-compiler-plugin
      configuration: {
            source: "1.8",
            target: "1.8"}

Créer un projet
-----------

jvn init [template] crée le fichier yml

Developpement
============
tasks

[ ] Plugin Runnable Parametrable (interraction sur Project par un singleton Javen)
[ ] load any *.javen.* file
[ ] modules parsing
[ ] manage plugin
[ ] manage git (.gitignore, git init, git creation protocol)
[ ] template
[ ] init
[ ] xml2yml

[ ] Secure private mvn repository based on ssh keys or https keys 


Axe de réflexion
==========

wrapper un dump bidir xml-yaml sur un seul arbre "maven"
ou sur tous les arbres racines pour simplifier ("build" "plugins", "profiles" properties ...)

Extension futures
 - graddle (en java)


langages/Types de projets
 - java/ web, standalone, desktop
 - xtend, ceylon, kotlin /*
 - rust?
 - go?
 - python (pip)
 - C (install des apt-get)


 outils non traités (pdm trop faibles)
 buildr
 
 
Repositories
============
Integration of new reposirories should be easier

https://jitpack.io


https://stackoverflow.com/questions/7526814/is-there-any-free-online-maven-repository
https://repsy.io/

Architecture
============
- The Javen main class redirect all subcommands to method or Commend classes (to be refactored)
- The PomCommand is the default command and the purpose of that project
- The JavenProjectLoader is the engine for the pom command

Javen -> PomCommand
PomCommand -> JavenProjectLoader

Project Bean
------------
it represent the yaml attributes
all maven attributes are get in "raw mode" 
this one are specific:
	parent
	artifact
	include 
	compile 
	test 
	provided
	runtime 
this one are moved:
  plugins
that one are to be removed (duplicate problem)
  group
  artifactid
  version
that one are to be merged (for exclusion mgt)
 dependencies



Dependencies
------------
for instance: limited depdencies: org.json and JAXB.



Java class file version
-----------------------
There's a 44 gap

Java 11 uses major version 55
Java 17 uses major version 61
 
