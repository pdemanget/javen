package pdem.javen;

import java.util.Map;

import pdem.javen.bean.Artifact;
import pdem.javen.util.FooLogger;

public class JavenManager {
  public static final JavenManager instance = new JavenManager();
  public static final String JAVEN_EXTENSION = "javen.yml";
  public static final String VERSION_EXTENSION = "version";
  private static final char[] fileArtifactSeparators = "%_#~".toCharArray();
  public static final String JAVEN_INCLUDE = "javen.include";
  private FooLogger logger = new FooLogger();
  private Map<String,String> args;
  
  public static char[] getFileartifactseparators() {
    return fileArtifactSeparators;
  }

  public static String getArtifactName(String groupId, String artifactId, String version) {
    return groupId + ":" + artifactId + ":" + version;
  }

  private JavenManager() {
    
  }
  
  private String replaceAllChars(String s, char[] chars, char replaced) {
    for (char c : chars) {
      s = s.replace(c, replaced);
    }
    return s;
  }

  public Artifact fromFileName(String filename) {
    String artifact = replaceAllChars(filename, getFileartifactseparators(), ':');
    return fromArtifactName(artifact);
  }

  public Artifact fromArtifactName(String artifact) {
    String[] artifactTokens = artifact.split(":");
    Artifact result = new Artifact();
    switch (artifactTokens.length) {
    case 0:
      break;
    case 1:
      result.setGroupId(artifactTokens[0]);
      result.setArtifactId(artifactTokens[0]);
      break;
    default:
      result.setVersion(artifactTokens[2]);
    case 2:
      result.setGroupId(artifactTokens[0]);
      result.setArtifactId(artifactTokens[1]);
    }
    return result;
  }
  
  public String getFileName(String artifact) {
    logger.debug(args);
    boolean shortName=args.get("name").startsWith("s");
    boolean longName=args.get("name").startsWith("l");

    Artifact artifactName = fromFileName(artifact);
    String filenamePrefix=longName?artifactName.getGroupId()+"_"+artifactName.getArtifactId()+".":
      shortName?"":artifactName.getArtifactId()+".";
    return filenamePrefix+JAVEN_EXTENSION;
  }

  public Map<String, String> getArgs() {
    return args;
  }

  public void setArgs(Map<String, String> args) {
    this.args = args;
  }
  
  public void removeJavenAttr(Map<String, Object> map ) {
    // keep the rest as raw data (only for read - should be a subclass)
    String[] javenAttrs = {"artifact","description","name","parent","include","compile","test","runtime","provided","properties","plugins","dependencies",
    	// also remove the raw attributes that are backed by other values
    		"modelVersion","groupId","artifactId","version"};
    for (String attr:javenAttrs) {
      map.remove(attr);
    }
    if(map.get("build")!=null && map.get("build") instanceof Map) {
      ((Map)map.get("build")).remove("plugins");
    }
  }

}
