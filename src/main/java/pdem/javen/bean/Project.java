package pdem.javen.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Json root descriptor
 *
 */
// @JsonIgnoreProperties(ignoreUnknown = true)
public class Project {
	private String parent;
	private String artifact;
	private List<String> include = new ArrayList<>();
	/**
	 * compile dependencies
	 */
	private List<String> compile= new ArrayList<>();
	private List<String> test= new ArrayList<>();
	private List<String> provided= new ArrayList<>();
	private List<String> runtime= new ArrayList<>();
	// raw data
	private List<?> dependencies = new ArrayList<>(); // OLDList<String>
	private Map<String, String> properties = new HashMap<>();
	private String name;
	private String description;
	private List<?> plugins = new ArrayList<>();
	
	private Map<String,Object> raw;

	public String getParent() {
		return parent;
	}

	public String getArtifact() {
		return artifact;
	}

	public List getDependencies() {
		return dependencies;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public void setArtifact(String artifact) {
		this.artifact = artifact;
	}

	public void setDependencies(List list) {
		if(list == null) {list.clear();return;}
		
		this.dependencies = list;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List getPlugins() {
		return plugins;
	}

	public void setPlugins(List list) {
		if(list == null) {plugins.clear();return;}
		
		this.plugins = list;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> map) {
		if( map == null) {
			this.properties.clear();
			return;
		}
		this.properties = map;
	}

	public void setInclude(List<String> list) {
		if(list == null) {include.clear();return;}
		this.include = list;
	}

	public List<String> getInclude() {
		return include;
	}

	public List<String> getCompile() {
		return compile;
	}

	public void setCompile(List<String> list) {
		if(list == null) {compile.clear();return;}
		this.compile = list;
	}

	public List<String> getTest() {
		return test;
	}

	public void setTest(List<String> list) {
		if(list == null) {test.clear();return;}
		this.test = list;
	}

	public List<String> getProvided() {
		return provided;
	}

	public void setProvided(List<String> list) {
		if(list == null) {provided.clear();return;}
		this.provided = list;
	}

	public List<String> getRuntime() {
		return runtime;
	}

	public void setRuntime(List<String> list) {
		if(list == null) {runtime.clear();return;}
		this.runtime = list;
	}

  public Map<String, Object> getRaw() {
    return raw;
  }

  public void setRaw(Map<String, Object> raw) {
    this.raw = raw;
  }

}
