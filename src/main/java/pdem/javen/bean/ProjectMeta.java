package pdem.javen.bean;

import java.util.function.Function;

/**
 * Attribute name of project for reflection
 * @author fil
 */
public enum ProjectMeta {
  artifact(Project::getArtifact),
  description(Project::getDescription),
  name(Project::getName),
  parent(Project::getParent),
  include(Project::getInclude),
  compile(Project::getCompile),
  test(Project::getTest),
  runtime(Project::getRuntime),
  provided(Project::getProvided),
  dependencies(Project::getDependencies),
  properties(Project::getProperties),
  plugins(Project::getPlugins);
  private final Function<Project,Object> getter;

  private ProjectMeta(Function<Project, Object> getter) {
    this.getter = getter;
  }

  public Function<Project, Object> getGetter() {
    return getter;
  }
  
   
  
}
