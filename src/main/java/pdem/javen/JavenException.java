package pdem.javen;

public class JavenException extends RuntimeException {
	public JavenException(String msg, Throwable t){
		super(msg,t);
	}
	
	public JavenException(String msg){
		super(msg,null);
	}
}
