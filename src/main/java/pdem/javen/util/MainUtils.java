package pdem.javen.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Some utilities for command-line application.
 *
 * MainHelper loads commmand line arguments AND conf.properties in resources and current filepath.
 *
 */
public class MainUtils {

    protected static final String UTF_8 = "UTF-8";
    public static final  String ZARGS = "zargs";
	
    public static boolean isWindows() {
      return System.getProperty( "os.name" ).toLowerCase().indexOf( "win" ) >= 0;
  }


    /**
     * parse arguments nix-style.
     */
    public static Map<String,String> parseArgs( String[] args ) {
        Map<String,String> param = new HashMap<String,String>();
        String otherArgs = "";
        String sep = "";
        int argc=0;
        for ( int i = 0 ; i < args.length - 1 ; i++ ) {
            if ( args[ i ].startsWith( "--" ) ) {
                param.put( args[ i ].substring( 2 ), "" );
            }
            else if ( args[ i ].startsWith( "-" ) ) {
                param.put( args[ i ].substring( 1 ), args[ i + 1 ] );
                i++;
            }
            else {
                otherArgs += sep + args[ i ];//bouh, c'est mal, tant pis.
                param.put("arg" + argc,args[i]);
                argc++;
                sep = ",";
            }
        }
        if (args.length > 0) {
          if (args[args.length - 1].startsWith("--")) {

            param.put(args[args.length - 1].substring(2), "");
          } else {
            param.put("arg" + argc,args[args.length - 1]);
            otherArgs += sep + args[args.length - 1];// bouh, c'est mal, tant pis.
            sep = ",";
          }
        }
       param.put( ZARGS, otherArgs );
       return param;

    }

    public static Properties loadProperties( String pProp ) {
        if ( pProp == null ) {
            pProp = "conf.properties";
        }
        Properties lProp = new Properties();
        ClassLoader lClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            InputStream lResourceAsStream = lClassLoader.getResourceAsStream( pProp );
            if ( lResourceAsStream != null ) {
                lProp.load( lResourceAsStream );
            }
        }
        catch ( FileNotFoundException e ) {
            logError( "Warning, expected properties file:", pProp );
        }
        catch ( IOException e ) {
            logError( "IOException :", lProp, e );
        }
        return lProp;
    }

    public static Properties loadPropertiesFile( String pProp , String defaultValue) {
        if ( pProp == null ) {
            pProp = defaultValue;
        }
        Properties lProp = new Properties();
        if(pProp == null) {
          return lProp;
        }
        try {
            lProp.load( new FileInputStream( pProp ) );
        }
        catch ( FileNotFoundException e ) {
            logDebug( "Warning, seeking file:", pProp );
        }
        catch ( IOException e ) {
            logError( "IOException :", lProp, e );
        }
        return lProp;
    }

    private static StringBuilder getMsg( Object... pObjects ) {
        StringBuilder lMsg = new StringBuilder();
        for ( Object o : pObjects ) {
            if ( o != null ) {
                if ( o instanceof Throwable ) {
                    Throwable e = (Throwable)o;
                    StringWriter lStack = new StringWriter();
                    e.printStackTrace( new PrintWriter( lStack ) );
                    lMsg.append( e.getClass().getSimpleName() );
                    lMsg.append( ":" );
                    lMsg.append( e.getMessage() );
                    lMsg.append( '\n' );
                    lMsg.append( lStack );
                }
                lMsg.append( o );
                lMsg.append( ' ' );
            }
        }
        return lMsg;
    }

    public static void logDebug( Object... pObjects ) {
    }
    
    public static void logError( Object... pObjects ) {
        System.err.println( getMsg( pObjects ) );
    }



    public static void showUsage( String pAppName, String pUsage1, String pUsage2 ) {
        pAppName = ( pAppName + "                     " ).substring( 0, 20 );
        pUsage1 = ( pUsage1 + "                                                                                 " )
            .substring( 0, 75 );
        pUsage2 = ( pUsage2 + "                                                                                 " )
            .substring( 0, 75 );
        if ( isWindows() ) {
            logError(

            "����������������������������������������������������������������������������Ŀ\n"
                + "�                     " + pAppName + "  �                       �\n"
                + "� Usage :                                   ��������������������������������Ĵ\n"

                + "� " + pUsage1 + "�\n" + "� " + pUsage2 + "�\n"
                + "������������������������������������������������������������������������������\n" );
        }
        else {
            logError(

            "/----------------------------------------------------------------------------\\\n" + "|                  "
                + pAppName + "     |      2010                      |\n"
                + "| Usage :                                   \\--------------------------------|\n" + "| " + pUsage1
                + "|\n" + "| " + pUsage2 + "|\n"
                + "\\----------------------------------------------------------------------------/\n" );
        }

    }

    public static void putAll( Map<String,String> pDest, Properties pProp ) {
        for ( Object lO : pProp.keySet() ) {
            pDest.put( (String)lO, (String)pProp.get( lO ) );
        }
    }

    /**
     * Main delegation for parsing arguments and display usage.
     *  2 default arguments:
     *      -conf file the loaded configuration file
     *      --help display usage
     *
     *      zargs contains unnames arguments.
     */
    public static Map<String,String> mainHelper( String[] args, String pAppName, String pUsage1, String pUsage2  , String globalConfiguration) {
        try {
            Map<String,String> lParseArgs = MainUtils.parseArgs( args );
            Map<String,String> lParam = new HashMap<String,String>();
            Properties lLoadProperties = MainUtils.loadProperties( pAppName+".properties" );
            Properties lLoadProperties2 = MainUtils.loadPropertiesFile( lParseArgs.get( "conf" ),globalConfiguration );

            putAll( lParam, lLoadProperties );
            putAll( lParam, lLoadProperties2 );//les valeurs du fichiers ecrasent celle du properties
            lParam.putAll( lParseArgs );//les arguments du programme ecrase les valeurs des fichiers.
            if ( lParseArgs.get( "help" ) != null ) {
                MainUtils.showUsage( pAppName, pUsage1, pUsage2 );
                return null;
            }
            return lParam;

        }
        catch ( Exception e ) {
            MainUtils.showUsage( pAppName, pUsage1, pUsage2 );
            MainUtils.logError( e );
            return null;
        }

    }
}
