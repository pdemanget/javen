package pdem.javen.util;

/**
 * Simple console logger with zero mandatory config. 
 * @author fil
 */
public class FooLogger {
	/**
	 * TRACE 0
	 * DEBUG 1
	 * INFO 2
	 * WARN 3
	 * ERROR 4
	 */
	int level=0;
	public FooLogger() {
		String slevel=System.getProperty("FooLogger.level");
		if(slevel != null) {
			try {
				level=Integer.parseInt(slevel);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void trace(Object ...msg) {
		log(0,msg);
	}

	public void debug(Object ...msg) {
		log(1,msg);
	}
	
	public void info(Object ...msg) {
		log(2,msg);
	}
	public void warn(Object ...msg) {
		log(3,msg);
	}
	public void error(Object ...msg) {
		log(4,msg);
	}

	private void log(int i, Object... args) {
		if(i<level) return;
		String msg="";
		for(Object arg:args) {
			msg+=arg.toString();
		}
		System.out.println(msg);
	}

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

}
