package pdem.javen.util;

/**
 * Run main method
 *
 * Inspired from SO and Javafx launcher.
 * http://stackoverflow.com/questions/939932/how-to-determine-main-class-at-runtime-in-threaded-java-application
 */
public abstract class MainLauncher implements Runnable{

  public String[] params;

/**
 * java.lang.Thread
pdem.javen.util.MainLauncher
pdem.javen.util.MainLauncher
pdem.javen.util.MainLauncher
pdem.javen.Javen

 * @return
 */
  @SuppressWarnings ("rawtypes")
  public static Class getCaller(){
    try {
      String className = Thread.currentThread().getStackTrace()[1].getClassName();
      int line=0;
      for( StackTraceElement ste:Thread.currentThread().getStackTrace()) {
    	className=ste.getClassName();
    	if( line !=0 && ! MainLauncher.class.getName().equals(className)) {
    		break;
    	}
    	line++;
      }
      Class<?> clazz = Class.forName(className);
      return clazz;
    } catch (ClassNotFoundException e) {
      throw new IllegalStateException(e);
    }
  }

  public static String getMainClass() {
    String cmd = System.getProperty("sun.java.command");
    String[] args = cmd.split(" ");
    System.out.println(args);
    return args[0]; // like "org.x.y.Main arg1 arg2"
}


  /**
   * Guess inherited instance and start
   * @return
   */
  public static MainLauncher getInstance(){
    try {
//      String className = getMainClass();
//      System.out.println(className);
//      System.out.println(getCaller());
//      Class<?> clazz = Class.forName(className);
    	Class<?> clazz = getCaller();
      return (MainLauncher) clazz.newInstance();
    } catch ( InstantiationException | IllegalAccessException e) {
      throw new IllegalStateException(e);
    }
  }


  public static void start(String[] args) {
    getInstance().setParams(args).run();

  }
  public static void main(String[] args) {
    start(args);
  }

  public String[] getParams() {
    return params;
  }

  public MainLauncher setParams(String... params) {
    this.params = params;
    return this;
  }
}
