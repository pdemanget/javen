package pdem.javen.util;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;


/**
 * KISS File and stream accessor.
 * All defaults to UTF-8
 * @author fil
 */
public class StreamUtils {

	public static void in2out(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[2048];
		int read=0;
		while( (read=in.read(buffer))>0) {
			out.write(buffer,0,read);
		}
	}
	
	public static String toString(InputStream in) throws IOException {
		// StringWriter out = new StringWriter();
	  // BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		in2out(in,out);
		return out.toString(StandardCharsets.UTF_8.name());
	}
	
	public static void writeFile(Path path, String s) throws IOException {
	  try(Writer writer = new OutputStreamWriter( new FileOutputStream(path.toFile()), StandardCharsets.UTF_8 )){
	    writer.write(s);
	  }
  }
	
	public static String readFile(Path path) throws IOException {
    return toString(Files.newInputStream(path));
  }
}
