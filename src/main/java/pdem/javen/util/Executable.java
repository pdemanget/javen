package pdem.javen.util;

public interface Executable {
  public static void main(String[] args) {
    MainLauncher.start(args);
  }
}
