package pdem.javen.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Generic Utils for data types: Collection and Maps
 * @author fil
 */
public class DataTypeUtils {
	
 public static <T> T dflt(T value,T defaultValue) {
	 return value==null?defaultValue:value;
 }

 public static Map<String,String> mapOf(String... values){
   Map<String,String> result = new HashMap<>();
   
   for(int i=0;i<values.length;i+=2) {
     result.put(values[i],values[i+1]);
   }
   return result;
 }
}
