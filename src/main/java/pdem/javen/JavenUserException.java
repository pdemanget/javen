package pdem.javen;

/**
 * Theres an exception because of the user, tell him politely.
 * @author fil
 */
public class JavenUserException extends RuntimeException {
  final String userMessage;
  
	public JavenUserException(String userMessage,String msg){
		super(msg,null);
		this.userMessage = userMessage;
	}
	
	public JavenUserException(String userMessage,String msg, Throwable t){
		super(msg,t);
		this.userMessage = userMessage;
	}

  public String getUserMessage() {
    return userMessage;
  }
}
