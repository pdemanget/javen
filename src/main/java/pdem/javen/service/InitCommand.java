package pdem.javen.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import pdem.javen.JavenException;
import pdem.javen.JavenManager;
import pdem.javen.bean.Artifact;
import pdem.javen.util.FooLogger;

/**
 * Init project with failover to folder name if no group or artifact are provided
 * @author fil
 *
 */
public class InitCommand implements Command{
  FooLogger logger = new FooLogger();

  @Override
  public String exec(Map<String, String> args) {
      String artifactArg = args.get("arg1");
      boolean shortName=args.get("name").startsWith("s");
      boolean longName=args.get("name").startsWith("l");

      
      if (artifactArg == null) {
        Path dir = Paths.get(".");
        try {
          // TODO the group?
          Path realPath = dir.toRealPath();
          artifactArg=realPath.getFileName().toString();
          
        } catch (IOException e) {
          throw new JavenException("can't find artifact",e);
        }
      }
      Artifact artifactName = JavenManager.instance.fromFileName(artifactArg);
      
      logger.debug(artifactArg);
      try {
        List<String> fileContent = new ArrayList<>();
        if(! longName) {
          fileContent.add("artifact: "+artifactName.keyWithoutVersion());
        }
        
        fileContent.addAll(Arrays.asList( 
            "# just an example of dependencies, feel free to uncomment"
            ,"#compile:"
//            ," - \"com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.7.1\"" 
//            ," - \"org.json:json:20160810\""
            ,"# - \"pl.droidsonroids.yaml:snakeyaml:1.18.2\""
            ) );
        String filenamePrefix=longName?artifactName.getGroupId()+":"+artifactName.getArtifactId()+".":
          shortName?"":artifactName.getArtifactId()+".";
        
        Files.write(Paths.get(filenamePrefix+JavenManager.JAVEN_EXTENSION),fileContent);
        Files.write(Paths.get(filenamePrefix+JavenManager.VERSION_EXTENSION),Arrays.asList( 
            "0.0.1-SNAPSHOT"
            ));
        Files.createDirectories(Paths.get("src/main/java"));
        Files.createDirectories(Paths.get("src/main/resources"));
        Files.createDirectories(Paths.get("src/test/java"));
        Files.createDirectories(Paths.get("src/test/resources"));

      } catch (IOException e) {
        throw new JavenException("can't write jvn conf file",e);
        
      }
      //pom();
      return artifactArg;
    }


}
