package pdem.javen.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;

import org.json.JSONObject;
import org.yaml.snakeyaml.Yaml;

import pdem.javen.util.MainLauncher;

/**
 * Bidirectional converter.
 */
public class YamlJsonConverter extends MainLauncher implements Runnable{

  public String yaml2Json(String yamlString) {
    Yaml yaml= new Yaml();
    Map<String,Object> map= (Map<String, Object>) yaml.load(yamlString);

    JSONObject jsonObject=new JSONObject(map);
    return jsonObject.toString(2);
  }

  public String json2Yaml(String json) {
    JSONObject jsonObject=new JSONObject(json);
    Map<String, Object> map = jsonObject.toMap();
    Yaml yaml= new Yaml();
    return yaml.dumpAsMap(map);
  }

  
  public void run() {
   try {
     Map<String,Object> prj = (Map<String,Object>) new Yaml().load(new FileInputStream("example.javen.yml"));
    JSONObject jsonObject=new JSONObject(prj);
    System.out.println(jsonObject.toString(2));
  } catch (FileNotFoundException e) {
    e.printStackTrace();
  }

  }

  public static void main(String[] args) {
    //start(args);
    System.out.println(new YamlJsonConverter(). json2Yaml("{\"a\":\"A\"}"));
  }


}
