package pdem.javen.service;

import static pdem.javen.JavenManager.JAVEN_EXTENSION;
import static pdem.javen.JavenManager.VERSION_EXTENSION;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.yaml.snakeyaml.Yaml;

import pdem.javen.JavenException;
import pdem.javen.JavenManager;
import pdem.javen.JavenUserException;
import pdem.javen.bean.Project;
import pdem.javen.util.StreamUtils;

public class JavenProjectLoader {
  private JavenManager javenManager = JavenManager.instance;

  public Project readConfig(Map<String, Object> root ) {
  Project project = new Project();
    // manual parse of JsonObject
    project.setArtifact((String) root.get("artifact"));
    project.setDescription((String) root.get("description"));
    project.setName((String) root.get("name"));
    project.setParent((String) root.get("parent"));
    
    project.setInclude((List<String>) (List<?>) root.get("include"));
    project.setCompile((List<String>) (List<?>) root.get("compile"));
    project.setTest((List<String>) (List<?>) root.get("test"));
    project.setRuntime((List<String>) (List<?>) root.get("runtime"));
    project.setProvided((List<String>) (List<?>) root.get("provided"));

    // push raw data from dynamic XML
    project.setProperties((Map<String,String>) root.get("properties"));
    project.setPlugins((List<?>) root.get("plugins"));

    return project;

  }

  public Project readJson(Path config) throws IOException {
    String json;
    byte[] bytes = Files.readAllBytes(config);
    json = new String(bytes, "UTF-8");
    JSONObject root = new JSONObject(json);
    Project readConfig = readConfig(root.toMap());
  return readConfig;
  }

  @SuppressWarnings("unchecked")
public Project readYaml(Path config) throws IOException {
    Yaml yaml = new Yaml();
    Map<String, Object> map = (Map<String, Object>) yaml.load(new FileInputStream(config.toFile()));
    Project project = readConfig(map);
    includeProjets(yaml, project);
    generatedArtifact(config, project);
    generatedVersion(config, project);
    
    javenManager.removeJavenAttr(map);
    project.setRaw(map);
    return project;
  }

  private void includeProjets(Yaml yaml, Project project) throws IOException {
    if (project.getInclude() != null) {
      for (String elt : project.getInclude()) {
        String projectResource = elt + ".javen.yml";
        InputStream resourceInclude = getClass().getClassLoader().getResourceAsStream(projectResource);
        if(resourceInclude == null) {
          throw new JavenUserException("Include not found:"+resourceInclude,"Can't find resource in depedencies:"+projectResource);
        }
        String inc = StreamUtils.toString(resourceInclude);
        Project incPrj = readConfig((Map<String, Object>) yaml.load(inc));

        includeProject(project, incPrj);
      }
    }
  }

  public Project includeProject(Project project ,Project included ) {
    project.getCompile().addAll(included.getCompile()); 
    project.getTest().addAll(included.getTest()); 
    project.getRuntime().addAll(included.getRuntime()); 
    project.getProvided().addAll(included.getProvided()); 
    project.getProperties().putAll(included.getProperties()); 
    project.getPlugins().addAll(included.getPlugins()); 
      return project;
   }

  
  private String replaceAllChars(String s, char[] chars, char replaced) {
    for (char c : chars) {
      s = s.replace(c, replaced);
    }
    return s;
  }

  private void generatedArtifact(Path config, Project project) {
    if (project.getArtifact() == null) {
      String filename = config.getFileName().toString();
      if (filename.length() < JAVEN_EXTENSION.length()) {
        throw new JavenException("project artifact is mandatory", null);
      }
      // patch missing artifact
      String filenameWithoutExt = filename.substring(0, filename.length() - JAVEN_EXTENSION.length() - 1);
      String artifact = replaceAllChars(filenameWithoutExt, javenManager.getFileartifactseparators(), ':');
      project.setArtifact(artifact);
    }
  }

  protected void generatedVersion(Path config, Project project) throws IOException {
    if (project.getArtifact().split(":").length <= 2) {
      String filename = config.getFileName().toString();
      String prefix = filename.substring(0, filename.length() - JAVEN_EXTENSION.length());
      Path versionFile = config.getParent().resolve(prefix + VERSION_EXTENSION);
      if (!versionFile.toFile().exists()) {
        throw new JavenException("version is mandatory");
      }
      String version = new String(Files.readAllBytes(versionFile), StandardCharsets.UTF_8).trim();
      project.setArtifact(project.getArtifact() + ":" + version);
    }
  }

  /**
   * find javen.yml file
   * @param projectFolder
   * @return
   */
  public Path findConfigFile(Path projectFolder) {
    try {
      List<Path> find = Files.list(projectFolder).filter(f -> {
        return !Files.isDirectory(f) && f.toString().endsWith(JAVEN_EXTENSION);
      }).collect(Collectors.toList());

      if (find.size() == 1) {
        return find.get(0);
      }
      if (find.size() == 0) {
        throw new JavenUserException("No javen to process", "No Javen to process with extension " + JAVEN_EXTENSION);
      }
      throw new JavenUserException("More than one javen to process " + find.size() + " files found",
          "many files found " + find);

    } catch (IOException e) {
      throw new JavenException("Can't find config file", e);
    }
  }

}
