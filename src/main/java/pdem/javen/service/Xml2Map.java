package pdem.javen.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Reverse maven to Javen.
 * 
 * Loads XML file to DOM model.
 * Transform DOM to generic Map.
 * 
 * 
 * 
 * Comments are either ignored, or trapped in temporary format (TBD)
 * 
 * @author pdemanget
 */
public class Xml2Map {

  public Map<String, Object> getMap(Element elt) {
	  System.out.println("Reversion XML to simplified Map");
    HashMap<String, Object> result = new LinkedHashMap<>();
    
    for (int i = 0; i < elt.getChildNodes().getLength(); i++) {
      Node node = elt.getChildNodes().item(i);
      String nodeName = node.getNodeName();

      if ("#text".equals(nodeName)) {
        continue;
      }
      if ("#comment".equals(nodeName)) {
        continue;
      }
      if (isArray(node)) {
        result.put(nodeName, getArray(node));
      } else {
        Object value = getValue(node);
        result.put(nodeName, value);
      }
    }
    return result;
  }

  private Object getArray(Node elt) {
    List result = new ArrayList();
    for (int i = 0; i < elt.getChildNodes().getLength(); i++) {
      Node node = elt.getChildNodes().item(i);
      Object value = getValue(node);
      if(node.getNodeType()==Node.COMMENT_NODE) {
        // TODO comments cannot be placed in Map (or need to be hacked in yaml serialisation) result.add("# "+value);
      }else if (! "".equals(value)) {
    	  result.add(value);
      }
    }
    return result;
  }

  private Object getValue(Node node) {
    if (node.hasChildNodes()&& node.getChildNodes().getLength()>1 
   // 		&& !node.getFirstChild().getNodeName().equals("#text")
    		) {
      return getMap((Element) node);
    }
    return node.getTextContent().trim();
  }

  private boolean isArray(Node node) {
	  // TODO better algorithm: loop on nodes and filter on valid nodename, if more than one, array
	//  System.out.println("NodeName:"+node.getNodeName()+".");
    boolean array = "dependencies".equals(node.getNodeName()) || 
    		"plugins".equals(node.getNodeName()) ||
    		"modules".equals(node.getNodeName());
    // System.out.println(array);
    return array;
  }

  public Document loadXml(String file) throws SAXException, IOException, ParserConfigurationException{
    File fXmlFile = new File (file);
    
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(fXmlFile);
    return doc;
  }

  
  
  public Object safeSearch(Map<String, Object> root,String path) {
    String[] keys=path.split("\\.");
    Object result = root;
    for(String key:keys) {
      if(((Map<String, Object>)result).get(key)==null) {
        return null;
      }
      result = ((Map<String, Object>) result).get(key);
    }
    return result;
  }


}
