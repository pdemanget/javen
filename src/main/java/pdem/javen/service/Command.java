package pdem.javen.service;

import java.util.Map;

public interface Command {

  String exec(Map<String,String> args);
}
