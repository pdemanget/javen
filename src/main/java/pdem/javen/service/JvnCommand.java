package pdem.javen.service;

import static pdem.javen.JavenManager.JAVEN_INCLUDE;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.representer.Representer;

import pdem.javen.Javen;
import pdem.javen.JavenManager;
import pdem.javen.bean.Project;
import pdem.javen.bean.ProjectMeta;

/**
 * Reverse generate javen file.
 * 
 * @author fil
 */
public class JvnCommand  implements Command{
  private Xml2Map xml2Map = new Xml2Map();
  private JavenManager javenManager= JavenManager.instance;

  public Project toProject(Map<String, Object> root) {
    Project project = new Project();
    project.setArtifact(
        javenManager.getArtifactName((String) root.get("groupId"), (String) root.get("artifactId"), (String) root.get("version")));
	if (root.get("dependencies") != null) {
		for (Map dep : (List<Map>) root.get("dependencies")) {
			if (dep.get("exclusions") != null) {
				project.getDependencies().add(dep);
			} else {
				String scope = (String) dep.get("scope");
				if (scope == null)
					scope = "compile";
				String artifact = dep.get("groupId") + ":" + dep.get("artifactId") + ":" + dep.get("version");// classifier
																												// and
																												// type
				switch (scope) {
				case "compile":
					project.getCompile().add(artifact);
					break;
				case "test":
					project.getTest().add(artifact);
					break;
				case "runtime":
					project.getRuntime().add(artifact);
					break;
				case "provided":
					project.getProvided().add(artifact);
					break;
				}
			}
		}
    }

    // copy raw data
    project.setDescription((String) root.get("description"));
    project.setName((String) root.get("name"));
    project.setParent((String) root.get("parent"));

    // push raw data from dynamic XML
    project.setProperties((Map<String, String>) root.get("properties"));
    project.setPlugins((List) xml2Map.safeSearch(root, "build.plugins"));

    // keep additional javen data

    String javenInclude = project.getProperties().get(JAVEN_INCLUDE);
    if (javenInclude != null) {
      String[] include = javenInclude.split(",");
      project.setInclude(Arrays.asList(include));
      project.getProperties().remove(JAVEN_INCLUDE);
    }
    javenManager.removeJavenAttr(root);
    project.setRaw(root);

    return project;
  }
  
  public Project toProject(String pomFile) throws SAXException, IOException, ParserConfigurationException {
    // load raw XML
    Document loadXml = xml2Map.loadXml(pomFile);

    // convert to generic raw Map
    Map<String, Object> map = xml2Map.getMap(loadXml.getDocumentElement());
    
    // convert generic Map to Javen project
    Project project = toProject(map);
    return project;
  }

  public void toJaven(String pomFile) throws SAXException, IOException, ParserConfigurationException {
    Project project = toProject(pomFile);
    String dump = toYaml(project);
    
    Path ymlPath = Paths.get(javenManager.getFileName(project.getArtifact()) );
    Files.write(ymlPath, dump.getBytes(StandardCharsets.UTF_8));
    // if -o std System.out.println(dump);
  }

  private String toYaml(Project project) {
    DumperOptions dumperOptions = new DumperOptions();
    dumperOptions.setPrettyFlow(false);
    Representer representer = new Representer();
    Node node = representer.represent(null);
    // TODO deal with comment
    //node.set
    
    Yaml yaml = new Yaml(representer, dumperOptions);
    
    //String dump = yaml.dumpAsMap(project);
    //push back data to raw
    Map result = new LinkedHashMap<>();
    result.putAll(project.getRaw());
    
    for(ProjectMeta meta:ProjectMeta.values()) {
      result.put(meta.name().toLowerCase(),meta.getGetter().apply(project));
    }
    String dump = yaml.dumpAsMap(result);
    return dump;
  }

  @Override
  public String exec(Map<String, String> args) {
    try {
      toJaven("pom.xml");
    } catch (SAXException | IOException | ParserConfigurationException e) {
      e.printStackTrace();
    }
    return "";
  }

}
