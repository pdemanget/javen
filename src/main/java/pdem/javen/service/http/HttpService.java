package pdem.javen.service.http;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpService {

	OkHttpClient client = new OkHttpClient();

	// code request code here
	byte[] get(String url) {
		Request request = new Request.Builder().url(url).build();
		try {
			Response response = client.newCall(request).execute();
			return response.body().bytes();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	String upload(String url, byte[] data) {
		Request request = new Request.Builder()
				.url(url)
				.post(RequestBody.create(MediaType.parse("application/json"),data))
				.build();
		try {
			Response response = client.newCall(request).execute();
			return response.body().string();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) {
		String res = new String(new HttpService().get("http://localhost:8001/index.html"));
		System.out.println(res);
	}

}
