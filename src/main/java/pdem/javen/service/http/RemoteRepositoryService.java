package pdem.javen.service.http;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pdem.javen.JavenManager;
import pdem.javen.bean.Artifact;


/**
 * Access to maven repository.
 * example:
 * https://repo1.maven.org/maven2/org/hibernate/hibernate-core/5.4.1.Final/ 
 * 
 * @author pdemanget
 */
public class RemoteRepositoryService {
	private List<String> remotes = new ArrayList<>();
	
	
	
	public RemoteRepositoryService() {
		super();
		remotes.add("https://repo1.maven.org/maven2");
	}

	HttpService service = new HttpService();

	// code request code here
	byte[] get(String artifact) {
		byte[] res;
		for(String remote :remotes) {
			return service.get(remote+getUrl(artifact));
		}
		return null;
// fv<sfZ RFZ rfz FAE GAH3)r,épiz,		service.upload(getUrl(artifact),data);
	}

	private String getUrl(String artifactName) {
		Artifact artifact = JavenManager.instance.fromArtifactName(artifactName);
		return artifact.getGroupId().replace('.','/')+"/"+artifact.getArtifactId()+"/"+artifact.getVersion()
		+"/"+artifact.getArtifactId()+"-"+artifact.getVersion()+"."+artifact.getType();
	}

	public static void main(String[] args) {
		//String res = new RemoteRepositoryService().get("http://localhost:8001/index.html");
		//System.out.println(res);
	}

}
