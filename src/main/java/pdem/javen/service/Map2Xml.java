package pdem.javen.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.yaml.snakeyaml.Yaml;

import pdem.javen.bean.Project;
import pdem.javen.util.StreamUtils;

/**
 * Map2XML
 * convert any Map coming from Json or yaml to xml.
 * Also convert arrays to tag list, wrapped by main key:
 *
 * foos:[1,2,3]
 *
 * main key is foos, xml result is :
 * <foos>
 * <foo>1</foo>
 * <foo>2</foo>
 * <foo>3</foo>
 * </foos>
 */
public class Map2Xml {

  private Document doc;
  
  
  public static void yml2xml(Path src,Path dest) throws IOException {
	    Yaml yaml = new Yaml();
	    Map<String, Object> map = (Map<String, Object>) yaml.load(new FileInputStream(src.toFile()));
	    Document doc = genericToXml(map);
	    try {
			serializeXml(doc, dest);
		} catch (TransformerException | TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
	  }

  public static Document genericToXml(Map map) {
	    //XmlMapper xml = new XmlMapper();
	    DocumentBuilderFactory xmlFactory = DocumentBuilderFactory.newInstance();
	    try {
	      DocumentBuilder xmlBuilder = xmlFactory.newDocumentBuilder();
	      Document doc = xmlBuilder.newDocument();
	      
	      Map2Xml json2Xml = new Map2Xml(doc);
	      Element mainRootElement = doc.createElement( "project");
	      doc.appendChild(mainRootElement);

	      json2Xml.mapToXml(mainRootElement,map);
	      
	      return doc;

	    } catch (ParserConfigurationException e) {
	      e.printStackTrace();
	      return null;
	    }
	  }
  private static void serializeXml(Document doc, Path dest) throws TransformerConfigurationException, TransformerFactoryConfigurationError, TransformerException {
	    //TO Console
	    Transformer transformer = TransformerFactory.newInstance().newTransformer();
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    DOMSource source = new DOMSource(doc);
	    StreamResult console = new StreamResult(dest.toFile());
	    transformer.transform(source, console);
	  }


  public Map2Xml(Document doc) {
    super();
    this.doc = doc;
  }

  public void  toXml(Element destRoot,Map<String,Object> obj){
    for(String key:obj.keySet()){
      Element destElt = doc.createElement(key);
      destRoot.appendChild(destElt);
      Object child = obj.get(key);
      objectToXml(key, destElt, child);
    }

  }

  public void  mapToXml(Element destRoot,Map<String,String> obj){
    if(obj == null) return;
    for(String key:obj.keySet()){
      Element destElt = doc.createElement(key);
      destRoot.appendChild(destElt);
      Object child = obj.get(key);
      objectToXml(key, destElt, child);
    }

  }

  
  @SuppressWarnings ({ "unchecked", "rawtypes" })
  private void objectToXml(String key, Element elt, Object child) {
    if(child instanceof Map){
      toXml(elt,(Map<String,Object>) child);
    }else if (child instanceof Iterable){
      toXml(elt, (Iterable) child,  key);
    } else{
      elt.setTextContent(child.toString());
    }
  }


  /**
   * Rewrap the list elements: dependencies contains dependency, plusing contains plugin
   * @param destRoot
   * @param obj
   * @param key
   */
  public void toXml(Element destRoot, @SuppressWarnings ("rawtypes") Iterable obj, String key) {
    //TODO Si le contenu est un objet " avec type Wrappé" il faudrait ajouter ce type, sinon on le devine par le nom d'array sans le "s".
    String destTag = key.substring(0,key.length()-1);
    if(destTag.equals("dependencie")) {
    	destTag="dependency";
    }
    for(Object child:obj){
      Element destElt = doc.createElement(destTag);
      destRoot.appendChild(destElt);
      objectToXml(destTag,destElt,child);
    }

  }

}
