package pdem.javen.service;

import static pdem.javen.JavenManager.JAVEN_INCLUDE;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pdem.javen.JavenException;
import pdem.javen.JavenManager;
import pdem.javen.JavenUserException;
import pdem.javen.bean.Project;
import pdem.javen.util.DataTypeUtils;
import pdem.javen.util.FooLogger;

/**
 * pom.xml generation. Main command of the project
 *
 * <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi=
 * "http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation=
 * "http://maven.apache.org/POM/4.0.0
 * http://maven.apache.org/xsd/maven-4.0.0.xsd">
 * <modelVersion>4.0.0</modelVersion>
 *
 */
public class PomCommand implements Command {
	private FooLogger logger = new FooLogger();
	private JavenManager javenManager = JavenManager.instance;
	private JavenProjectLoader javenProjectLoader = new JavenProjectLoader();

	// public void run() {
	// Project value = readConfig();
	// writeDom(value);
	// }

	public void writeDom(Project project, Path folder) {
		// XmlMapper xml = new XmlMapper();
		folder=folder==null?Paths.get("."):folder;
		DocumentBuilderFactory xmlFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder xmlBuilder = xmlFactory.newDocumentBuilder();
			Document doc = xmlBuilder.newDocument();
			Element mainRootElement = doc.createElementNS("http://maven.apache.org/POM/4.0.0", "project");
			mainRootElement.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xsi",
					"http://www.w3.org/2001/XMLSchema-instance");
			mainRootElement.appendChild(createNode(doc, "modelVersion", "4.0.0"));
			doc.appendChild(mainRootElement);

			String[] artifact = project.getArtifact().split(":");
			mainRootElement.appendChild(createNode(doc, "groupId", artifact[0]));
			mainRootElement.appendChild(createNode(doc, "artifactId", artifact[1]));
			mainRootElement.appendChild(createNode(doc, "version", artifact[2]));
			mainRootElement.appendChild(createNode(doc, "name", project.getName()));
			mainRootElement.appendChild(createNode(doc, "description", project.getDescription()));

			Element properties = doc.createElement("properties");
			mainRootElement.appendChild(properties);
			Map2Xml json2Xml = new Map2Xml(doc);
			project.getProperties().put(JAVEN_INCLUDE, String.join(",", project.getInclude()));

			json2Xml.mapToXml(properties, project.getProperties());

			Element dependencies = doc.createElement("dependencies");
			mainRootElement.appendChild(dependencies);
			json2Xml.toXml(dependencies, project.getDependencies(), "dependencies");

			addDeps(doc, dependencies, project.getCompile(), "compile");
			addDeps(doc, dependencies, project.getTest(), "test");
			addDeps(doc, dependencies, project.getRuntime(), "runtime");
			addDeps(doc, dependencies, project.getProvided(), "provided");

			json2Xml.toXml(mainRootElement, project.getRaw());
			Element build = null;
			Element plugins = null;
			build = domLazyAdd(doc, mainRootElement,"build");
			plugins = domLazyAdd(doc, build,"plugins");
			json2Xml.toXml(plugins, project.getPlugins(), "plugins");
			serializePom(doc, folder);

			return;
		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
	}

	private Element domLazyAdd(Document doc, Element mainRootElement, String tagname) {
		Element build;
		NodeList elementsByTagName = mainRootElement.getElementsByTagName(tagname);
		if (elementsByTagName.getLength()!=0) {
			build = (Element) elementsByTagName.item(0);
		} else {
			build = doc.createElement(tagname);
			mainRootElement.appendChild(build);			
		}
		return build;
	}

	private void addDeps(Document doc, Element dependencies, List<String> compile, String scope) {
		if (compile == null)
			return;
		for (String dep : compile) {
			String[] dependency = dep.split(":");
			dependencies.appendChild(createNode(doc, "dependency", createNode(doc, "groupId", dependency[0]),
					createNode(doc, "artifactId", dependency[1]), createNode(doc, "version", dependency[2]),
					createNode(doc, "scope", scope)));
		}
	}

	private void serializePom(Document doc, Path folder)
			throws TransformerConfigurationException, TransformerFactoryConfigurationError, TransformerException {
		// TO Console
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		DOMSource source = new DOMSource(doc);
		logger.debug("write pom to ", folder);
		StreamResult console = new StreamResult(folder.resolve("pom.xml").toFile() );
		transformer.transform(source, console);
	}

	private void checkAdd(Project project, Project included, Function<Project, List> getter) {
		List includedList = getter.apply(included);
		if (includedList != null) {
			List projectList = getter.apply(project);
//		if(projectList == null) {
		}

	}

	private Element createNode(Document doc, String tag, String value) {
		Element createElement = doc.createElement(tag);
		createElement.setTextContent(value);
		return createElement;
	}

	private Element createNode(Document doc, String tag, Element... elements) {
		Element createElement = doc.createElement(tag);
		for (Element element : elements) {
			createElement.appendChild(element);
		}
		return createElement;
	}

	/**
	 * f: jvn file p: path of config file, defaulted to current
	 */
	@Override
	public String exec(Map<String, String> args) {
		try {
			PomCommand generatePom = new PomCommand();
			Path config = args.get("f") != null 
					? Paths.get(args.get("f"))
					: javenProjectLoader.findConfigFile(Paths.get(DataTypeUtils.dflt(args.get("p"), ".")));
			System.err.println("Using Javen");
			Project project = javenProjectLoader.readYaml(config);
			logger.debug(config);
			generatePom.writeDom(project, config.getParent());
		} catch (JavenUserException e) {
			System.err.println(e.getUserMessage() + "\nDetails:\n\t" + e.getMessage());
		} catch (IOException | JavenException e) {
			e.printStackTrace();
		}
		return "";
	}

}
