package pdem.javen;

import static pdem.javen.util.StreamUtils.readFile;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.yaml.snakeyaml.Yaml;

import pdem.javen.service.InitCommand;
import pdem.javen.service.JvnCommand;
import pdem.javen.service.Map2Xml;
import pdem.javen.service.PomCommand;
import pdem.javen.service.Xml2Map;
import pdem.javen.service.YamlJsonConverter;
import pdem.javen.util.FooLogger;
import pdem.javen.util.MainLauncher;
import pdem.javen.util.MainUtils;
import pdem.javen.util.StreamUtils;

/**
 * wraps pom.xml in a yaml.
 * transformed attributes are
 * artifact: string (to groupid artifactid, version)
 * dependencies: list<string>
 * @author pdemanget
 */
public class Javen extends MainLauncher {
	public static final String VERSION="0.0.7";
	
	public FooLogger logger= new FooLogger();;
	private JavenManager javenManager=JavenManager.instance;

	@Override
	public void run() {
		Map<String, String> parsedArgs = MainUtils.mainHelper(getParams(), "javen", "javen [pom,jvn,init,xml2yml,yml2xml]", "javen Version "+VERSION,"/opt/etc/javen.properties");
		javenManager.setArgs(parsedArgs);
		//logger.debug(""+parsedArgs);
		if (parsedArgs != null && parsedArgs.get("arg0") != null) {
			String cmd = parsedArgs.get(MainUtils.ZARGS);
			List<String> cmds = Arrays.asList( cmd.split(","));
			if(cmds.contains("package") || cmds.contains("test")|| cmds.contains("install")) {
				pom(parsedArgs);
				return;
			}
			switch (parsedArgs.get("arg0")) {
			case "pom":
				pom(parsedArgs);
				break;
			case "jvn":
				jvn(parsedArgs);
				break;
			case "init":
				String res = new InitCommand().exec(parsedArgs);
				System.out.println(res);
				break;
			case "xml2yml":
				xml2yml(parsedArgs);
				break;
			case "yml2xml":
				yml2xml(parsedArgs);
				break;
      case "yml2json":
        yml2json(parsedArgs);
        break;
      case "json2yml":
        json2yml(parsedArgs);
        break;
			default:
				System.err.println("Javen: nothing to do");;
			}
		}else {
			pom(parsedArgs);
		}
	}

  private void json2yml(Map<String, String> parsedArgs) {
    try {
      StreamUtils.writeFile(Paths.get(parsedArgs.get("arg2")), 
        new YamlJsonConverter().json2Yaml(readFile(Paths.get( parsedArgs.get("arg1")))));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

   private void yml2json(Map<String, String> parsedArgs) {
      try {
        StreamUtils.writeFile(Paths.get(parsedArgs.get("arg2")), 
            new YamlJsonConverter().yaml2Json(readFile(Paths.get( parsedArgs.get("arg1")))));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void yml2xml(Map<String, String> parsedArgs) {
		try {
			Map2Xml.yml2xml(Paths.get(parsedArgs.get("arg1")) ,
					Paths.get(parsedArgs.get("arg2"))
					);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void xml2yml(Map<String, String> parsedArgs) {
		Xml2Map xml2Map = new Xml2Map();
		try {
			System.out.println("Reading file "+parsedArgs.get("arg1"));
      Document xml = xml2Map.loadXml(parsedArgs.get("arg1"));
      Map<String, Object> map = xml2Map.getMap(xml.getDocumentElement());
      Yaml yaml = new Yaml();
      String dump = yaml.dumpAsMap(map);
      System.out.println(dump);
      
    } catch (SAXException | IOException | ParserConfigurationException e) {
      e.printStackTrace();
    }
	}

	public void pom(Map<String, String> args) {
	  new PomCommand().exec(args);
	}
	
	/**
	 *  reverse the XML to jvn
	 * 
	 */
	public void jvn(Map<String,String> args) {
			new JvnCommand().exec(args);
	}


		

	public static void main(String[] args) {
		start(args);
	}



}
