package pdemanget.javen2;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.maven.pom._4_0.Dependency;
import org.apache.maven.pom._4_0.Model;
import org.apache.maven.pom._4_0.Model.Dependencies;
import org.apache.maven.pom._4_0.Parent;
import org.w3c.dom.Element;

import pdem.javen.JavenManager;
import pdem.javen.bean.Artifact;
import pdem.javen.bean.Project;
import pdem.javen.service.Command;
import pdem.javen.service.JavenProjectLoader;
import pdem.javen.util.DataTypeUtils;

public class Pom2Command implements Command {
  private JavenProjectLoader javenProjectLoader = new JavenProjectLoader();

  @Override
  public String exec(Map<String, String> args) {
    Path config = args.get("f") != null ? Paths.get(args.get("f"))
        : javenProjectLoader.findConfigFile(Paths.get(DataTypeUtils.dflt(args.get("p"), ".")));

    Project project;
    try {
      project = javenProjectLoader.readYaml(config);
    } catch (IOException e) {
      e.printStackTrace();
      return "error";
    }

    Model model = new Model();
    model.setParent(getParent(project.getParent().split(":")));
    Artifact artifact = JavenManager.instance.fromFileName(project.getArtifact());

    model.setGroupId(artifact.getGroupId());
    model.setVersion(artifact.getVersion());
    model.setArtifactId(artifact.getArtifactId());

    model.setDescription(project.getDescription());
    model.setName(project.getName());

    // model.getProperties().getAny().addAll(project.getProperties().entrySet().stream().map(e->new
    // Element()).collect(Collectors.toList()));
    model.setDescription(project.getDescription());

    Dependencies dep = new Dependencies();

    model.setDependencies(dep);
    addDependencies(dep, project.getCompile(), "compile");
    addDependencies(dep, project.getTest(), "test");
    addDependencies(dep, project.getProvided(), "provided");
    addDependencies(dep, project.getRuntime(), "runtime");

    return "pom.xml generated with artifactName "+artifact;
  }

  private void addDependencies(Dependencies dep, List<String> compile, String scope) {
    for (String artifactName : compile) {
      Artifact artifact = JavenManager.instance.fromArtifactName(artifactName);

      Dependency dependency = new Dependency();
      dep.getDependency().add(dependency);
      dependency.setArtifactId(artifact.getArtifactId());
      dependency.setGroupId(artifact.getGroupId());
      dependency.setVersion(artifact.getVersion());
      dependency.setScope(scope);

    }
  }

  private Parent getParent(String[] parentArtifact) {
    Parent parent = new Parent();
    parent.setGroupId(parentArtifact[0]);
    parent.setArtifactId(parentArtifact[1]);
    parent.setVersion(parentArtifact[2]);
    return parent;
  }

  private Parent getArtifact(String[] artifact) {
    Parent parent = new Parent();
    parent.setGroupId(artifact[0]);
    parent.setArtifactId(artifact[1]);
    parent.setVersion(artifact[2]);
    return parent;
  }

}
