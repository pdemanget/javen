@Grab('com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.7.1')
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParser;

/**

 groovy test.groovy {a:true}
*/

//def map = new HashMap();
//map.put("a","aaa")
def mapper = new ObjectMapper()
   mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
def map = mapper.readValue(args[0],Map.class);

println "Hello world! "+map

/*
{
	artifact:'groovy.test:groovy.test:1.0.0-SNAPSHOT',
	template:'java',
	dep: [
	 'com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.7.1'
	],
	depVersions: {
	 'com.fasterxml.jackson.datatype:jackson-datatype-jsr310':'2.7.1'
	}
}

