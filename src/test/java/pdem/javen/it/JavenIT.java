package pdem.javen.it;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import pdem.javen.Javen;
import pdem.javen.util.DataTypeUtils;
import pdem.javen.util.StreamUtils;

public class JavenIT {

	@ValueSource(strings= {"test.jvn","it/blue.javen.yml"})
	@ParameterizedTest
	public void testPom( String jvnFile) throws IOException {
		String resFolder = "target/test-classes/";
		// WHEN
		new Javen().pom(DataTypeUtils.mapOf("f", resFolder + jvnFile));
		
		String pom = StreamUtils.readFile(Paths.get(resFolder + "pom.xml"));
		Path javenPath = Paths.get(resFolder + jvnFile);
		String fileName = javenPath.getFileName().toString();
		String expectedPom = StreamUtils.readFile(javenPath.getParent().resolve(fileName + ".pom.xml"));
		Assertions.assertEquals(pom, expectedPom);
	}
	
	@ValueSource(strings= {"javen.yml"})
	@ParameterizedTest
	public void testReal( String jvnFile) throws IOException {
		// WHEN
		new Javen().pom(DataTypeUtils.mapOf("f",  jvnFile));
		
	
	}


}
