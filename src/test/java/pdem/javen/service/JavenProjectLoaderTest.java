package pdem.javen.service;
import java.io.IOException;
import java.nio.file.Paths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import pdem.javen.bean.Project;

public class JavenProjectLoaderTest {
	
	@Test
	public void test1ReadYaml() throws IOException {
		JavenProjectLoader generatePom = new JavenProjectLoader();
		Project project = generatePom.readYaml(Paths.get("target/test-classes/test1.javen.yml"));
		Assertions.assertNotNull(project);
	}
	
	@Test
	public void test2ReadYaml() throws IOException {
    JavenProjectLoader generatePom = new JavenProjectLoader();
		Project project = generatePom.readYaml(Paths.get("target/test-classes/test2.javen.yml"));
		Assertions.assertNotNull(project);
	}
	
	@Test
	public void testGeneratedVersion() throws IOException {
    JavenProjectLoader generatePom = new JavenProjectLoader();
		Project project = new Project();
		project.setArtifact("foo:bar");
		generatePom.generatedVersion(Paths.get("target/test-classes/foo%bar.javen.yml"), project);
		Assertions.assertEquals("foo:bar:0.0.0",project.getArtifact());
	}

}
