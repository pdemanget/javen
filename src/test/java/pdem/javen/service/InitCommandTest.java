package pdem.javen.service;

import org.junit.jupiter.api.Test;

import pdem.javen.util.DataTypeUtils;

public class InitCommandTest {
  
  @Test
  public void testExec() {
    new InitCommand().exec(DataTypeUtils.mapOf("name","short"));
  }
  
}
