package pdem.javen.service;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.yaml.snakeyaml.Yaml;

import pdem.javen.bean.Project;


/**
 * reverse pom.xml to javen
 * @author philippe
 */
public class ReverseTest {
	
	@Test
	public void testMap2xml() throws IOException, SAXException, ParserConfigurationException {
	  JvnCommand generator = new JvnCommand();
	  Project project = generator.toProject("target/test-classes/reverse/pom.xml");
	  Assertions.assertEquals(project.getArtifact(),"pdem:javen:0.0.1-SNAPSHOT");
	
	}
	
	@Test
	public void testModule() throws IOException, SAXException, ParserConfigurationException {
	  JvnCommand generator = new JvnCommand();
	  Project project = generator.toProject("target/test-classes/reverse/module/pom.xml");
	  Assertions.assertEquals(project.getArtifact(),"com.acm:foo:1.2.1");
	
	}
	
	@Test
  public void test1ReadYaml() throws IOException, SAXException, ParserConfigurationException {
    Xml2Map xml2Map = new Xml2Map();
    Document loadXml = xml2Map.loadXml("pom.xml");
    Map<String, Object> map = xml2Map.getMap(loadXml.getDocumentElement());
    JvnCommand generator = new JvnCommand();
    Project project = generator.toProject(map);
    Yaml yaml = new Yaml();
    String dump = yaml.dumpAsMap(project);
    System.out.println(dump);
    Assertions.assertNotNull(project);
  }

}
