package pdem.javen.service;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;


public class Map2XmlTest {
	
	@Test
	public void testMap2xml() throws IOException {
		Map2Xml.yml2xml(Paths.get("target/test-classes/test1.javen.yml"),
				Paths.get("target/test.xml"));
	}
	
	@Test
	public void test2() throws IOException {
		Map2Xml.yml2xml(Paths.get("src/test/resources/it/yml2xml/test.yml"),
				Paths.get("target/test2.xml"));
	}

}
