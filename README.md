Javen
=====

		"Javen, the maven precompiler, yaml format."
		
Javen create Maven file from Yaml or Json format. 
It doesn't replace or wrap Maven, it is just launched before maven via mavenrc.
It can also be launched separatly with jvn pom


Simple usage demo
-----------------




Syntax example
--------------

    artifact: "pdem:example:0.0.1-SNAPSHOT"
    name: Project example
    properties:
      mainClass: foo.HelloWorld
    compile:
     - "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.7.1"
     - "org.json:json:20160810"
     - "pl.droidsonroids.yaml:snakeyaml:1.18.2"
    plugins:
     -  groupId: org.apache.maven.plugins
        artifactId: maven-compiler-plugin
        configuration:
              source: "1.8"
              target: "1.8"


Kick-start
----------
Here are the basic transformations of the pom.xml: 
 - bidirectionnal raw transformation from-to xml-yml 
 - new yaml attributes for a more synthetic file


new attributes:
 - compile :  dependencies with scope=compile
 - test :  dependencies with scope=test
 - runtime :  dependencies with scope ... you get it.
 - provided : dependencies
 - artifact : short version of groupid artifactid version
 
All dependencies and artifact are written with : separated syntax: "pl.droidsonroids.yaml:snakeyaml:1.18.2"

existing pom attributes that are reused with a basic xml to yaml transformation:
 - plugins
 - properties
 
 


see doc/javen.md

Why?
---
I just wanted a "DRY" version of maven, whithout repeating plugins. I also like to remove useless xml files whenever I can.
